#!/bin/bash

## update and upgrade ##
yes | sudo apt update && sudo apt upgrade

## remove bloat ##
yes | sudo apt autoremove libreoffice libreoffice-common

## install minimal text editor
yes | sudo apt install geany

## install git & ansible
yes | sudo apt install git ansible

## install cool stuff
yes | sudo apt install htop cool-retro-term 

#### install regolith
# sudo add-apt-repository ppa:regolith-linux/release
# yes | sudo apt install regolith-desktop-complete

## install spetrwm and extras
yes | sudo apt install spectrwm nitrogen pcmanfm arandr

## install virt-manager and configure
yes | sudo apt install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils virt-manager

sudo usermod -aG libvirt $USER
sudo usermod -aG kvm $USER

